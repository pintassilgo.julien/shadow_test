window.onload = function () {

	// OUVERTURE/FERMETURE DU FORMULAIRE
	document.getElementById("accept").addEventListener("click", function(e) {
		document.getElementById('form').style.display = "block";
		document.getElementById('accept').style.color = "#2196F3";
		document.getElementById('bg').style.filter = "blur(5px)";
	});

	document.getElementById("close").addEventListener("click", function(e) {
		document.getElementById('form').style.display = "none";
		document.getElementById('accept').style.color = "#4e4e4e";
		document.getElementById('bg').style.filter = "blur(0px)";
	});

	document.getElementById("decline").addEventListener("click", function(e) {
		document.getElementById('form').style.display = "none";
		document.getElementById('accept').style.color = "#4e4e4e";
		document.getElementById('bg').style.filter = "blur(0px)";
		document.getElementById('popupDecline').style.display = "flex";
		document.getElementById('overlay').style.display = "block";
	});

	document.getElementById("annuler").addEventListener("click", function(e) {
		document.getElementById('popupDecline').style.display = "none";
		document.getElementById('overlay').style.display = "none";
	});

	var menuBool = false;

	// OUVERTURE/FERMETURE DU MENU
	document.getElementById("burger").addEventListener("click", function() {
		if (!menuBool) {
			document.getElementById('main').style.transform = "translateX(40rem)";
			document.getElementById('bg').style.transform = "translateX(40rem)";
			document.getElementById('menu').style.display = "block";
			document.getElementById('menu').style.boxShadow = "inset -3px 0px 11px -2px rgba(0,0,0,0.75)";
			document.getElementById('menu').style.backgroundColor = "#333333";

			// ROTATION DU BURGER MENU
			var burgerSpan = document.getElementById('burger').getElementsByClassName('burgerSpan');
			console.log(burgerSpan.length);
			for (var i = 0; i < burgerSpan.length; i++) {
				burgerSpan[i].style.transform = "rotate(90deg)";
				burgerSpan[i].style.marginTop = "2.3rem";

				if (i == 0) {
					burgerSpan[i].style.marginLeft = "-1rem";
				}
				else if (i == 2) {
					burgerSpan[i].style.marginLeft = "1rem";
				}

			}
			menuBool = true;
		}
		else {
			document.getElementById('main').style.transform = "translateX(0rem)";
			document.getElementById('bg').style.transform = "translateX(0rem)";
			document.getElementById('menu').style.zIndex = "-20";
			document.getElementById('menu').style.boxShadow = "none";
			document.getElementById('menu').style.backgroundColor = "transparent";

			// ROTATION DU BURGER MENU
			var burgerSpan = document.getElementById('burger').getElementsByClassName('burgerSpan');
			console.log(burgerSpan.length);
			for (var i = 0; i < burgerSpan.length; i++) {
				burgerSpan[i].style.transform = "rotate(0deg)";
				burgerSpan[i].style.marginLeft = "0rem";

				if (i == 0) {
					burgerSpan[i].style.marginTop = "1.3rem";
				}
				else if (i == 1) {
					burgerSpan[i].style.marginTop = "2.3rem";
				}
				else if (i == 2) {
					burgerSpan[i].style.marginTop = "3.3rem";
				}

			}
			menuBool = false;
		}
	})
}